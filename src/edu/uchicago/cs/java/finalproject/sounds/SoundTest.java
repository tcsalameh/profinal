package edu.uchicago.cs.java.finalproject.sounds;

import javax.sound.sampled.*;

/**
 * Created by tcsalameh on 11/22/15.
 */
public class SoundTest {

    public static void main(String[] args) throws InterruptedException {

        Clip clp = Sound.loadClip("strings/Bowgart C3.wav");

        Sound.playSound(clp);
        Thread.sleep(50);
        Sound.playSound(clp);

    }
}
