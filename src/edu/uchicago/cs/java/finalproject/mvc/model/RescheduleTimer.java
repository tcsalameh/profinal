package edu.uchicago.cs.java.finalproject.mvc.model;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by tcsalameh on 12/5/15.
 * http://stackoverflow.com/questions/32001/resettable-java-timer
 * This code for a reschedulable timer is based on the link above, I use it in the repeater to
 * account for changes in tempo.
 */
public class RescheduleTimer extends Timer {
    // needs to have a timer
    // method to create, discard, and start new timertask objects
    private Runnable task;
    private TimerTask timerTask;

    public void schedule(Runnable runnable, long delay) {
        task = runnable;
        timerTask = new TimerTask() {
            @Override
            public void run() {
                task.run();
            }
        };
        this.scheduleAtFixedRate(timerTask,delay,delay);
    }

    public void reschedule(long delay) {
        timerTask.cancel();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                task.run();
            }
        };
        this.scheduleAtFixedRate(timerTask,delay,delay);
    }
}
