package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.nio.file.Paths;

public class Keys extends Instrument {
    private static final int EXPIRE = 80; //expiry time
    private static final int RATE = 3; //expansion rate
    private static final Color COLOR = new Color(30,100,179);
    private static final String[] SOUNDS = {Paths.get("keys","Celesta C2.wav").toString(), //C
            Paths.get("keys","Celesta Db2.wav").toString(), //Db
            Paths.get("keys","Celesta D2.wav").toString(), //D
            Paths.get("keys","Celesta Eb2.wav").toString(), //Eb
            Paths.get("keys","Celesta E2.wav").toString(), //E
            Paths.get("keys","Celesta F2.wav").toString(), //F
            Paths.get("keys","Celesta Gb2.wav").toString(), //Gb
            Paths.get("keys","Celesta G2.wav").toString(), //G
            Paths.get("keys","Celesta Ab2.wav").toString(), //Ab
            Paths.get("keys","Celesta A2.wav").toString(), //A
            Paths.get("keys","Celesta Bb2.wav").toString(), //Bb
            Paths.get("keys","Celesta B2.wav").toString(), //B
            Paths.get("keys","Celesta C3.wav").toString(), //C
            Paths.get("keys","Celesta Db3.wav").toString(), //Db
            Paths.get("keys","Celesta D3.wav").toString(), //D
            Paths.get("keys","Celesta Eb3.wav").toString()}; //Eb

    /**
     * Constructor
     * @param p
     */
    public Keys(Point p, Clip clp) {
        super(p, COLOR, EXPIRE, RATE);
        setTeam(Team.FRIEND);
        if (clp == null)
            clp = Sound.loadClip(SOUNDS[super.getBucket()]);
        super.clip = clp;
        Sound.playSound(clp);
    }

    @Override
    public String[] getSounds() {
        return SOUNDS;
    }

    public Clip reloadClip() {
        return Sound.loadClip(SOUNDS[super.getBucket()]);
    }
}
