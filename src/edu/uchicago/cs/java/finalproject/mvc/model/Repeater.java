package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by tcsalameh on 11/20/15.
 */
public class Repeater<T extends Instrument> implements Movable {
    private boolean runIndicator; // while thread is running
    private boolean markerOn; // if marker is shown
    private Class<T> type; // type of instrument
    private int recurrenceRate; // how many bars we should wait before repeating
    private TimeSignature ts; // time signature so we can calculate ms per bar
    private int mBpm; // beats per minute
    private int recurInterval; // how many milliseconds to wait before repeating
    private Point location; // where we pressed on on the screen
    private int markerLocation; // for progress bar
    private Instrument i; // the actual instrument object this repeater constructs every cycle
    private Clip clp1; // sound clip to play, so we can preload it
    private Clip clp2; //backup clip

    /**
     * Constructor for a repeater object.
     * @param type Type of sound/instrument to be repeated
     * @param loc Coordinate location of instrument object
     * @param recurs How often it should occur (e.g. 1=once every measure, 2=once every 2 measures...)
     * @param t time signature
     * @param bpm beats per minute
     */

    public Repeater(Class<T> type, Point loc, int recurs, TimeSignature t, int bpm, int marker, boolean markOn) {
        this.type = type;
        this.location = loc;
        this.ts = t;
        this.mBpm = bpm;
        this.markerLocation = marker;
        this.markerOn = markOn;

        runIndicator = true;
        setRecurrenceRate(recurs);
        newNote(); // start with a note right away, this also loads the clip
        clp1 = i.clip; // get clip from the note just created so we can re-use it
        clp2 = i.reloadClip(); // also load a backup clip at same time
        animate();
    }

    public void markOn(boolean on) {
        this.markerOn = on;
    }

    public void setRecurrenceRate(int r) {
        if (r > 0)
            this.recurrenceRate = r;
        else
            this.recurrenceRate = 1;
        this.recurInterval = ts.getMillisecsPerBar(mBpm)*recurrenceRate;
    }

    public void setBpm(int bpm) {
        if (bpm < 1)
            this.mBpm = 1;
        else
            this.mBpm = bpm;
        this.recurInterval = ts.getMillisecsPerBar(mBpm)*recurrenceRate;
    }

    public int getRecurrenceRate() {return this.recurrenceRate;}

    public Class getInstrumentType() {return this.type;}

    /**
     * This constructs a new Instrument instance, and also pre-loads the clip if
     * it's not there already.
     */
    public void newNote() {
        // first we get the constructor for the Instrument we're making
        Constructor[] k = type.getConstructors();
        try {
            // then we create a new instrument.
            if (clp1 != null && clp1.isRunning()) {
                i = (T) k[0].newInstance(location, clp2);
            }
            else if (clp2 != null && clp2.isRunning()) {
                i = (T) k[0].newInstance(location, clp1);
            }
            else {
                i = (T) k[0].newInstance(location, clp1);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * This actually does the work of repeating the note/instrument.
     */
    public void animate() {
        //trying with a timer
        RescheduleTimer timer = new RescheduleTimer();
        Runnable task = new Runnable() {
            private int initRecur = recurInterval;

            @Override
            public void run() {
                if (runIndicator) {
                    if (initRecur != recurInterval) {
                        initRecur = recurInterval;
                        timer.reschedule(recurInterval);
                    }
                    newNote();
                } else {
                    // once we're done with the thread, close out clips to free up resources
                    Sound.closeClip(clp1);
                    Sound.closeClip(clp2);
                    timer.cancel();
                }
            }
        };

        timer.schedule(task,recurInterval);
    }

    /**
     * To shutdown the timer, we just set the run indicator to false.
     */
    public void shutdown() {
        // http://stackoverflow.com/questions/3777772/how-to-stop-a-thread-in-a-threadpool
        runIndicator = false;
    }

    @Override
    public void move() {
        // use the instrument's move method
        i.move();
    }

    @Override
    public void draw(Graphics g) {
        // need a new member that is set at the progress location when made
        // also need to set color to inst color
        // then draw it as a fillRect
        i.draw(g);
        if (markerOn) {
            Color c = i.getColor();
            g.setColor(c);
            int y = i.getMarkerLoc();
            g.fillRect(markerLocation, y, 3, 10);
        }

    }

    @Override
    public Point getCenter() {
        return this.location;
    }

    @Override
    public int getRadius() {
        return 0;
    }

    @Override
    public Team getTeam() {
        return Team.FRIEND;
    }
}
