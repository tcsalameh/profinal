package edu.uchicago.cs.java.finalproject.mvc.model;

import java.awt.*;

/**
 * Created by tcsalameh on 11/26/15.
 */
public class NoteIndicator extends Sprite {
    private Point c;
    private boolean hide;
    private InstrumentType inst;
    private static final Font NOTE_FNT = new Font("SansSerif", Font.PLAIN, 12);
    private static final String[] NOTE_VALS = {"C",
                                              "C#",
                                              "D",
                                              "D#",
                                              "E",
                                              "F",
                                              "F#",
                                              "G",
                                              "G#",
                                              "A",
                                              "A#",
                                              "B",
                                              "C",
                                              "C#",
                                              "D",
                                              "D#"};

    private static final String[] DRUM_VALS = {"Kick 1",
                                                "Kick 2",
                                                "Tom",
                                                "Ride",
                                                "Snare",
                                                "HiHat (closed)",
                                                "HiHat (open)",
                                                "Clap 1",
                                                "Clap 2",
                                                "Clap 3",
                                                "Clap 4",
                                                "Crash",
                                                "Crackle",
                                                "Glitch 1",
                                                "Glitch 2",
                                                "Ambience"};


    public NoteIndicator(Point p, InstrumentType t) {
        this.c = p;
        this.inst = t;
        this.setTeam(Team.FLOATER);
        this.hide = false;
    }

    public void hideShow() {
        this.hide = !this.hide;
    }

    public void setInstType(InstrumentType t) {
        this.inst = t;
    }

    @Override
    public void move() {

    }

    @Override
    public void draw(Graphics g) {
        if (!this.hide) {
            int x = this.getCenter().x;
            int y = this.getCenter().y;
            g.setFont(NOTE_FNT);
            g.setColor(Color.GREEN);
            String str = NOTE_VALS[getMouseRegion()];
            if (inst.equals(InstrumentType.PERCUSSION))
                str = DRUM_VALS[getMouseRegion()];
            g.drawString(str, x+5, y-5);
        }
    }

    public int getMouseRegion() {
        int xMap = getCenter().x - 1;
        int yMap = getCenter().y - 1;
        int xBinSize = getDim().width/4;
        int yBinSize = getDim().height/4;
        int xBin = xMap/xBinSize;
        int yBin = yMap/yBinSize;
        return (xBin + yBin*4);
    }
}
