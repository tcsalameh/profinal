package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.sounds.Sound;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;


public class Cc {

	private boolean bPlaying;
	private boolean bPaused;
	private TimeSignature timeSig;
	private int bpm;
	
	// These ArrayLists with capacities set
	private List<Movable> movDebris = new ArrayList<Movable>(300);
	private List<Movable> movFriends = new ArrayList<Movable>(100);
	private List<Movable> movFoes = new ArrayList<Movable>(200);
	private List<Movable> movFloaters = new ArrayList<Movable>(50);

	private GameOpsList opsList = new GameOpsList();

	//added by Dmitriy
	private static Cc instance = null;

	// Constructor made private - static Utility class only
	private Cc() {}


	public static Cc getInstance(){
		if (instance == null){
			instance = new Cc();
		}
		return instance;
	}

	public  void initGame(TimeSignature ts, int bpm){
		setBpm(bpm);
		setTimeSig(ts);
	}

	public int getRecurrenceInterval(Class iType) {
		for (Movable i : movFriends) {
			try {
				if (((Repeater) i).getInstrumentType().equals(iType)) {
					int interval = ((Repeater) i).getRecurrenceRate();
					return interval;
				}
			} catch (ClassCastException e) {
				e.printStackTrace();
			}
		}
		return 1;
	}

	public void setBpm(int bpm) {
		for (Movable i : movFriends) {
			try {
				if (i.getClass().equals(Repeater.class)) {
					((Repeater) i).setBpm(bpm);
				}
			} catch (ClassCastException e) {
				e.printStackTrace();
			} finally {
				this.bpm = bpm;
			}
		}
	}

	/**
	 * Originally had this method to set interval for ALL existing instances of an instrument.
	 * However this had unpredictable effects and also didn't allow for layering of multiple length
	 * phrases, so I didn't use it.
	 * @param iType
	 * @return
	 */
	public void setRecurrenceInterval(Class iType, int interval) {
		if (interval < 1)
			interval = 1;
		for (Movable i : movFriends) {
			try {
				if (((Repeater) i).getInstrumentType().equals(iType)) {
					((Repeater) i).setRecurrenceRate(interval);
				}
			} catch (ClassCastException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Dequeue last element in the list
	 */
	public Movable popLastFriend() {
		if (movFriends.size() > 0) {
			Movable m = movFriends.get(movFriends.size()-1);
			movFriends.remove(movFriends.size()-1);
			return m;
		}
		return null;
	}


	public GameOpsList getOpsList() {
		return opsList;
	}

	public void setOpsList(GameOpsList opsList) {
		this.opsList = opsList;
	}

	public  void clearAll(){
		movDebris.clear();
		movFriends.clear();
		movFoes.clear();
		movFloaters.clear();
	}

	public TimeSignature getTimeSig() {
		if (timeSig == null)
			return new TimeSignature(4,4);
		return timeSig;
	}

	public void setTimeSig(TimeSignature timeSig) {
		this.timeSig = timeSig;
	}

	public int getBpm() {
		return bpm;
	}

	public  boolean isPlaying() {
		return bPlaying;
	}

	public  void setPlaying(boolean bPlaying) {
		this.bPlaying = bPlaying;
	}

	public  boolean isPaused() {
		return bPaused;
	}

	public  void setPaused(boolean bPaused) {
		this.bPaused = bPaused;
	}

	public  List<Movable> getMovDebris() {
		return movDebris;
	}



	public  List<Movable> getMovFriends() {
		return movFriends;
	}


	public  List<Movable> getMovFoes() {
		return movFoes;
	}


	public  List<Movable> getMovFloaters() {
		return movFloaters;
	}




}
