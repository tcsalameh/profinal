package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.nio.file.Paths;

/**
 * Created by tcsalameh on 11/21/15.
 */
public class Bass extends Instrument {
    private static final int EXPIRE = 80; //expiry time
    private static final int RATE = 3; //expansion rate
    private static final Color COLOR = new Color(100,50,200);
    private static final String[] SOUNDS = {Paths.get("bass","Adrenaline C3.wav").toString(), //C
            Paths.get("bass","Adrenaline C#3.wav").toString(), //C#
            Paths.get("bass","Adrenaline D3.wav").toString(), //D
            Paths.get("bass","Adrenaline D#3.wav").toString(), //D#
            Paths.get("bass","Adrenaline E3.wav").toString(), //E
            Paths.get("bass","AdrenalineF3.wav").toString(), //F
            Paths.get("bass","Adrenaline F#3.wav").toString(), //F#
            Paths.get("bass","Adrenaline G3.wav").toString(), //G
            Paths.get("bass","Adrenaline G#3.wav").toString(), //G#
            Paths.get("bass","Adrenaline A3.wav").toString(), //A
            Paths.get("bass","Adrenaline A#3.wav").toString(), //A#
            Paths.get("bass","Adrenaline B3.wav").toString(), //B
            Paths.get("bass","Adrenaline C4.wav").toString(), //C
            Paths.get("bass","Adrenaline C#4.wav").toString(), //C#
            Paths.get("bass","Adrenaline D4.wav").toString(), //D
            Paths.get("bass","Adrenaline D#4.wav").toString()}; //D#4

    /**
     * Constructor
     * @param p
     */
    public Bass(Point p, Clip clp) {
        super(p, COLOR, EXPIRE, RATE);
        setTeam(Team.FRIEND);
        if (clp == null)
            clp = Sound.loadClip(SOUNDS[super.getBucket()]);
        super.clip = clp;
        Sound.playSound(clp);
    }

    @Override
    public String[] getSounds() {
        return SOUNDS;
    }

    public Clip reloadClip() {
        return Sound.loadClip(SOUNDS[super.getBucket()]);
    }

    @Override
    public int getMarkerLoc() {
        return getDim().height - 50;
    }
}
