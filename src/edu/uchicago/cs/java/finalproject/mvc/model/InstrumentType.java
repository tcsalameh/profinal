package edu.uchicago.cs.java.finalproject.mvc.model;

/**
 * Created by tcsalameh on 11/26/15.
 */

/**
 * Enum below is to keep track of:
 * 	1. Which type of instrument we're on
 * 	2. The "index" of that instrument (for switching b/t them)
 * 	3. The number bars each note created will repeat (default is 1).
 */
public enum InstrumentType {KEYS (Keys.class, 0, 1),
    PERCUSSION (Percussion.class, 1, 1),
    BASS (Bass.class, 2, 1),
    STRINGS (Strings.class, 3, 1),
    LEAD (Lead.class, 4, 1);

    public final Class iType;
    public final int n;
    public int interval;

    InstrumentType(Class type, int n, int freq) {this.iType = type;
        this.n = n;
        this.interval = freq;}

    //http://stackoverflow.com/questions/6692664/how-to-get-enum-value-from-index-in-java
    public static InstrumentType byOrdinal(int ord) {
        for (InstrumentType i : InstrumentType.values()) {
            if (i.n == ord)
                return i;
        }
        return null;
    }

    public void setInterval(int freq) {
        if (interval < 1)
            this.interval = 1;
        else
            this.interval = freq;
    }

    @Override
    public String toString() {
        String inst = iType.toString();
        // regex to strip out all but the last subclass name
        inst = inst.replaceAll(".*[.]", "");
        return inst;
    }

}
