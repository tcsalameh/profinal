package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.nio.file.Paths;

/**
 * Created by tcsalameh on 11/21/15.
 */
public class Strings extends Instrument{
    private static final int EXPIRE = 50;
    private static final int RATE = 1;
    private static final Color COLOR = new Color(80,150,100);
    private static final String[] SOUNDS = {Paths.get("strings","Bowgart C3.wav").toString(), //C
            Paths.get("strings","Bowgart C#3.wav").toString(), //C#
            Paths.get("strings","Bowgart D3.wav").toString(), //D
            Paths.get("strings","Bowgart D#3.wav").toString(), //D#
            Paths.get("strings","Bowgart E3.wav").toString(), //E
            Paths.get("strings","Bowgart F3.wav").toString(), //F
            Paths.get("strings","Bowgart F#3.wav").toString(), //F#
            Paths.get("strings","Bowgart G3.wav").toString(), //G
            Paths.get("strings","Bowgart G#3.wav").toString(), //G#
            Paths.get("strings","Bowgart A3.wav").toString(), //A
            Paths.get("strings","Bowgart A#3.wav").toString(), //A#
            Paths.get("strings","Bowgart B3.wav").toString(), //B
            Paths.get("strings","Bowgart C4.wav").toString(), //C
            Paths.get("strings","Bowgart C#4.wav").toString(), //C#
            Paths.get("strings","Bowgart D4.wav").toString(), //D
            Paths.get("strings","Bowgart D#4.wav").toString()}; //D#2

    public Strings(Point p, Clip clp) {
        super(p, COLOR, EXPIRE, RATE);
        setTeam(Team.FRIEND);
        if (clp == null)
            clp = Sound.loadClip(SOUNDS[super.getBucket()]);
        super.clip = clp;
        Sound.playSound(clp);
    }

    @Override
    public String[] getSounds() {
        return SOUNDS;
    }

    @Override
    public void move() {
        super.move();
    }

    @Override
    public void draw(Graphics g) {
        Graphics2D gd = (Graphics2D) g; //cast to Graphics2D so we can manipulate stroke width
        //http://www.java-forums.org/new-java/20553-how-set-line-width-graphics-object.html

        //to get fade effect, we use same color but change alpha based
        //on how close we are to expiry
        float r = (float) (getColor().getRed()/255.0);
        float b = (float) (getColor().getBlue()/255.0);
        float gr = (float) (getColor().getGreen()/255.0);
        float alpha;

        // We want to fade in for the first third of the duration,
        // then fade out for the last 2/3, to mirror the way the string sound fades in and out.
        if (getExpire() > 2*EXPIRE/3)
            alpha = (float) (((double) Math.abs(EXPIRE-getExpire()))/(2*EXPIRE/3));
        else
            alpha = (float) (((double) getExpire())/(4*EXPIRE/3));

        gd.setColor(new Color(r, gr, b, alpha));

        super.drawGridLines(gd);

        // For strings, we fill in the whole bucket where the click occurred.
        int x = ((getCenter().x)/(getDim().width/4))*getDim().width/4;
        int y = ((getCenter().y)/(getDim().height/4))*getDim().height/4;
        gd.fillRect(x, y, getDim().width/4, getDim().height/4);
    }

    public Clip reloadClip() {
        return Sound.loadClip(SOUNDS[super.getBucket()]);
    }

    @Override
    public int getMarkerLoc() {
        return getDim().height - 40;
    }
}
