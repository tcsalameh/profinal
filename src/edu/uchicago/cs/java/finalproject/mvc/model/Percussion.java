package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.nio.file.Paths;
import java.util.Timer;

/**
 * Created by tcsalameh on 11/20/15.
 */
public class Percussion extends Instrument {
    private static final int EXPIRE = 20; //expiry time
    private static final int RATE = 5; //rate
    private static final Color COLOR = new Color(244,60,4);
    private final String[] SOUNDS = {Paths.get("percussion","Kick YouKnow.wav").toString(),
                                     Paths.get("percussion","Kick 808 2.wav").toString(),
                                     Paths.get("percussion","FloorTom AR70sTight V127 1.wav").toString(),
                                     Paths.get("percussion","RideBell Central V2.wav").toString(),
                                     Paths.get("percussion","Snare 70sDnB 1.wav").toString(),
                                     Paths.get("percussion","ClosedHH 8-Ball.wav").toString(),
                                     Paths.get("percussion","OpenHH 8-Ball.wav").toString(),
                                     Paths.get("percussion","Combo CleanGutter.wav").toString(),
                                     Paths.get("percussion","Clap AR60sEarly V127 1.wav").toString(),
                                     Paths.get("percussion","Clap AR60sEarly V127 2.wav").toString(),
                                     Paths.get("percussion","Clap AR70sOpen V127 2.wav").toString(),
                                     Paths.get("percussion","Crash 70sDnB.wav").toString(),
                                     Paths.get("percussion","Crackle Sutekh 1.wav").toString(),
                                     Paths.get("percussion","Glitch Neolithic.wav").toString(),
                                     Paths.get("percussion","Glitch Resurrection.wav").toString(),
                                     Paths.get("percussion","Ambience DistSuspend.wav").toString()};

    public Percussion(Point p, Clip clp) {
        super(p, COLOR, EXPIRE, RATE);
        setTeam(Team.FRIEND);
        // if we already have a pre-loaded sound,
        // use it to minimize latency
        if (clp == null)
            clp = Sound.loadClip(SOUNDS[super.getBucket()]);
        super.clip = clp;
        Sound.playSound(clp);

    }

    @Override
    public String[] getSounds() {
        return SOUNDS;
    }

    public Clip reloadClip() {
        return Sound.loadClip(SOUNDS[super.getBucket()]);
    }

    @Override
    public int getMarkerLoc() {
        return getDim().height - 60;
    }
}
