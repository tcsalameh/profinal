Here's some additional documentation/how to on this project, since it's a little
different from a typical game. The main file to run to launch this application is Game.java, under
src/edu/uchicago/cs/java/finalproject/mvc/controller/.

I called this a "Music Generator". Really, it's something like a drum machine, but with added "instruments" and
a different interface. I was inspired by the design of MPCs (https://en.wikipedia.org/wiki/Music_Production_Center)
and similar electronic instruments.

There are 5 instruments that can be played, which can be switched between with the left and right arrow keys.
The name of the current instrument appears at the top of the screen.

The screen is subdivided into 16 rectangular sections. Each one has a particular sound mapped to it, by instrument.
For instance, when the "Keys" instrument is selected, each of the 16 sections corresponds to a different note on a
chromatic scale (from C to D#) when clicked. When "Percussion" is selected, these are mapped to various drum or
percussive sounds.

When you click that region of the screen, it plays that note, and also sets it into a loop that will repeat that note.
This means that you can build up melodies or rhythms that repeat over time. You can also control how often notes you
create will repeat by instrument, by using the up and down arrow keys to adjust the number of musical bars in between
repetitions. For instance, you could create a series of percussion notes that repeat every single bar,
and then switch to the bass and create a 2-bar phrase to overlay on the drum loop. You could also have different
repetition frequencies for different notes on the same instrument, because when you change the bar length it only
applies to notes you create in the future (it doesn't retroactively change any existing notes).

Notes can be removed, starting with the one most recently added, by hitting the space bar. The tempo
can also be changed on the fly by pressing the left or right caret keys. In the top left of the screen, there
are 3 numbers: The time signature (which is always 4/4), the beats per minute, and the number of measures notes you
create will wait before repeating.

There is also an optional overlay (enabled by default) that shows the note values in green text next to the cursor,
and also displays a metronome and markers for the notes along the bottom of the screen. Each bar, the "metronome"
line sweeps across the bottom of the screen, and (ideally) touches each note marker as the note plays
(NOTE: this does not stay in sync with existing notes if you change the BPM, because of variability in loading times
for sounds and the difficulty of keeping everything synchronized while changing tempos).
Right now, these markers simply display every note instantiated, whether it is playing in that particular bar or not.
This overlay can be toggled on and off with the i key.

The most difficult part of this project was getting the timing within an acceptable range, and minimizing
delays in loading audio and playing notes.
I've since read that relying on the system clock is actually not sufficient to keep highly accurate musical time.
However, in testing I've found that notes generally stay in time with each other, and any degradation
or variation is not very noticeable.